DROP SCHEMA IF EXISTS kulturnemanifestacije;
CREATE SCHEMA kulturnemanifestacije DEFAULT CHARACTER SET utf8;
USE kulturnemanifestacije;

CREATE TABLE gradovi(
ptt VARCHAR(10),
naziv VARCHAR(20),
PRIMARY KEY(ptt)
);

CREATE TABLE manifestacije(
manif_id INT AUTO_INCREMENT,
naziv VARCHAR(30) NOT NULL,
broj_posetilaca INT,
ptt VARCHAR(10),
PRIMARY KEY (manif_id), 

FOREIGN KEY (ptt) REFERENCES gradovi(ptt)
	ON DELETE CASCADE
);

INSERT INTO gradovi VALUES ('21000', 'Novi Sad');
INSERT INTO gradovi VALUES ('11000', 'Beograd');
INSERT INTO gradovi VALUES ('24000', 'Subotica');
INSERT INTO gradovi VALUES ('18000', 'Nis');
INSERT INTO gradovi VALUES ('25250', 'Odzaci');

INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) VALUES ('Exit 2018',250000,'21000');
INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) VALUES ('Sterijino pozorje 2018',3000,'21000');
INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) VALUES ('Fest 2018',10000,'11000');
INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) VALUES ('Noc muzeja 2018',15000,'11000');
INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) VALUES ('Palicki filmski festival 2018',2000,'24000');
INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) VALUES ('Festival ulicnih sviraca 2018',8000,'21000');
INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) VALUES ('Nisville 2018',20000,'18000');
INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) VALUES ('Cacib 2018',300,'25250');