package kulturne_manifestacije.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import kulturne_manifestacije.model.Grad;
import kulturne_manifestacije.model.Manifestacija;

public class GradDAO {

	public static Grad getGradByPttBroj(Connection conn, String ptt) {
		Grad grad = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT naziv FROM gradovi WHERE ptt = '" + ptt + "'";
			
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				int index = 1;
				String naziv = rset.getString(index++);
				
				grad = new Grad(ptt, naziv);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return grad;
	}
	
	public static List<Grad> getAll(Connection conn){
		List<Grad> gradovi = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT * FROM gradovi";
			
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				String ptt = rset.getString(index++);
				String naziv = rset.getString(index++);
				
				List<Manifestacija> manifestacije = ManifestacijaDAO.getManifestacijeByGradPtt(conn, ptt);
				Grad grad = new Grad(ptt, naziv);
				grad.getManifestacije().addAll(manifestacije);
				gradovi.add(grad);
				
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return gradovi;
	}
	
	public static Grad getGradByNaziv(Connection conn, String naziv) {
		Grad grad = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT ptt FROM gradovi WHERE naziv = '" + naziv + "'";
			
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				int index = 1;
				String ptt = rset.getString(index++);
				
				grad = new Grad(ptt, naziv);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return grad;
	}
	
	public static boolean update(Connection conn, Grad grad) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE gradovi SET naziv = ? WHERE ptt = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, grad.getNaziv());
			pstmt.setString(index++, grad.getPtt());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static boolean delete(Connection conn, Grad grad) {
		Statement stmt = null;
		try {
			String update = "DELETE FROM gradovi WHERE ptt = " + grad.getPtt();

			stmt = conn.createStatement();
			return stmt.executeUpdate(update) == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
}
