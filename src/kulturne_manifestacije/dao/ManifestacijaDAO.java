package kulturne_manifestacije.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import kulturne_manifestacije.model.Grad;
import kulturne_manifestacije.model.Manifestacija;
import kulturne_manifestacije.ui.ApplicationUI;

public class ManifestacijaDAO {
	
	public static List<Manifestacija> getManifestacijeByGradPtt(Connection conn, String ptt){
		List<Manifestacija> manifestacije = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT m.manif_id, m.naziv, m.broj_posetilaca, m.ptt, g.naziv FROM manifestacije m JOIN gradovi g"
					+ " ON g.ptt = m.ptt WHERE g.ptt = '" + ptt + "'";
			
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				int index = 1;
				int manifId = rset.getInt(index++);
				String nazivManifestacije = rset.getString(index++);
				int brojPosetilaca = rset.getInt(index++);
				String pttBroj = rset.getString(index++);
				String nazivGrada = rset.getString(index++);
				
				Grad grad = new Grad(pttBroj, nazivGrada);
				Manifestacija manifestacija = new Manifestacija(manifId, nazivManifestacije, brojPosetilaca, grad);
				grad.getManifestacije().add(manifestacija);
				manifestacije.add(manifestacija);
				
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return manifestacije;
	}
	
	public static Manifestacija getManifestacijaById(Connection conn, int id) {
		Manifestacija manif = null;
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT m.naziv, m.broj_posetilaca, g.ptt, g.naziv FROM manifestacije m JOIN gradovi g ON m.ptt = g.ptt "
					+ "WHERE manif_id = ?";
			
			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				index = 1;
				String nazivManifestacije = rset.getString(index++);
				int brojPosetilaca = rset.getInt(index++);
				String ptt = rset.getString(index++);
				String nazivGrada = rset.getString(index++);

				Grad grad = new Grad(ptt, nazivGrada);
				manif = new Manifestacija(id, nazivManifestacije, brojPosetilaca, grad);
				
			}
		} catch(SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return manif;
	}
	
	public static List<Manifestacija> getAll(Connection conn){
		List<Manifestacija> manifestacije = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT m.manif_id, m.naziv, m.broj_posetilaca, m.ptt, g.naziv FROM manifestacije m JOIN gradovi g"
					+ " ON g.ptt = m.ptt";
			
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int manifId = rset.getInt(index++);
				String nazivManifestacije = rset.getString(index++);
				int brojPosetilaca = rset.getInt(index++);
				String pttBroj = rset.getString(index++);
				String nazivGrada = rset.getString(index++);
				
				Grad grad = new Grad(pttBroj, nazivGrada);
				Manifestacija manifestacija = new Manifestacija(manifId, nazivManifestacije, brojPosetilaca, grad);
				grad.getManifestacije().add(manifestacija);
				manifestacije.add(manifestacija);
				
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return manifestacije;
	
	}
	
	public static boolean add(Connection conn, Manifestacija manifestacija) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO manifestacije (naziv, broj_posetilaca, ptt) values (?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, manifestacija.getNaziv());
			pstmt.setInt(index++, manifestacija.getBrojPosetilaca());
			pstmt.setString(index++, manifestacija.getGrad().getPtt());
			
			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static boolean update(Connection conn, Manifestacija manifestacija) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE manifestacije SET naziv = ?, broj_posetilaca = ?, ptt = ? WHERE manif_id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, manifestacija.getNaziv());
			pstmt.setInt(index++, manifestacija.getBrojPosetilaca());
			pstmt.setString(index++, manifestacija.getGrad().getPtt());
			pstmt.setInt(index++, manifestacija.getId());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static boolean delete(Connection conn, int id) {
		Statement stmt = null;
		try {
			String update = "DELETE FROM manifestacije WHERE manif_id = " + id;

			stmt = conn.createStatement();
			return stmt.executeUpdate(update) == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static Manifestacija nadjiNajposecenijuManifestaciju(Connection conn) {
		Manifestacija man = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT manif_id, naziv, max(broj_posetilaca), ptt FROM manifestacije";
		
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int brojPosetilaca = rset.getInt(index++);
				String ptt = rset.getString(index++);
				
				Grad grad = GradDAO.getGradByPttBroj(ApplicationUI.getConn(), ptt);
				man = new Manifestacija(id, naziv, brojPosetilaca, grad);
			}
		
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return man;
	}
}
