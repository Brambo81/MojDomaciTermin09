package kulturne_manifestacije.ui;

import java.util.List;

import kulturne_manifestacije.dao.GradDAO;
import kulturne_manifestacije.model.Grad;
import kulturne_manifestacije.utils.PomocnaKlasa;

public class GradUI {

	public static void ispisSvihGradova() {
		List<Grad> sviGradovi = GradDAO.getAll(ApplicationUI.getConn());
		
		System.out.println();
		System.out.println("Spisak gradova: ");
		System.out.println();
		System.out.printf("%10s %-20s\n", "Ptt broj", "Naziv grada");
		System.out.println("========== ====================");
		for (Grad grad : sviGradovi) {
			System.out.printf("%10s %-20s\n", grad.getPtt(), grad.getNaziv());
		}
		System.out.println("---------- --------------------");
	}
	
	public static void izmenaPodatakaOGradu() {
		System.out.print("Unesi ptt broj grada:");
		String ptt = PomocnaKlasa.ocitajTekst();
		Grad grad = GradDAO.getGradByPttBroj(ApplicationUI.getConn(), ptt);
		if (grad == null) {
			System.out.println("Grad sa ptt brojem " + ptt + " ne postoji u evidenciji.");
			return;
		}
		System.out.println("Novi naziv grada: ");
		String noviNaziv = PomocnaKlasa.ocitajTekst();
		grad.setNaziv(noviNaziv);
		
		if (GradDAO.update(ApplicationUI.getConn(), grad)) {
			System.out.println("Podaci o gradu su uspesno izmenjeni.");
			return;
		}
		System.out.println("Doslo je do greske. Podaci o gradu nisu izmenjeni.");
	
	}
	
	public static void brisanjeGrada() {
		System.out.print("Unesi ptt broj grada:");
		String ptt = PomocnaKlasa.ocitajTekst();
		Grad grad = GradDAO.getGradByPttBroj(ApplicationUI.getConn(), ptt);
		if (grad == null) {
			System.out.println("Grad sa ptt brojem " + ptt + " ne postoji u evidenciji.");
			return;
		}
		if(PomocnaKlasa.ocitajOdlukuOPotvrdi("da obrisete grad " + grad.getNaziv() + "?") == 'Y') {
			if (GradDAO.delete(ApplicationUI.getConn(), grad)) {
				System.out.println("Grad je uspesno izbrisan.");
				return;
			}
		}
		System.out.println("Grad nije izbrisan.");
	}
	
}
