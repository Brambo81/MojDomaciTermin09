package kulturne_manifestacije.ui;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import kulturne_manifestacije.utils.PomocnaKlasa;

public class ApplicationUI {

	private static Connection conn;
	
	static {
		// otvaranje konekcije, jednom na pocetku aplikacije
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");
			// otvaranje konekcije
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/kulturnemanifestacije?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			// kraj aplikacije
			System.exit(0);
		}
	}
	
	public static void main(String[] args)  {
		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				GradUI.ispisSvihGradova();
				break;
			case 2:
				ManifestacijaUI.ispisSvihManifestacija();
				break;
			case 3:
				ManifestacijaUI.pretragaManifestacijePoId();
				break;
			case 4:
				ManifestacijaUI.izmenaNazivaManifestacije();
				break;
			case 5:
				String sP = System.getProperty("file.separator");
				File manifestacija = new File("."+sP+"data"+sP+"izvestaj.csv");
				try{
					ManifestacijaUI.snimiNajposecenijuUFajl(manifestacija);
				} catch (IOException e) {
					System.out.println("Greska pri snimanju fajla!");
				}
				break;
			case 6:
				String spR = System.getProperty("file.separator");
				File man = new File("."+spR+"data"+spR+"izvestaj.csv");
				try{
					ManifestacijaUI.ucitajNajposecenijuIzFajlaIIspisi(man);
				} catch (IOException e) {
					System.out.println("Greska pri citanju fajla!");
				}
				break;
			case 7:
				ManifestacijaUI.unosManifestacije();
				break;
			case 8:
				ManifestacijaUI.obrisiManifestaciju();
				break;
			case 9:
				GradUI.izmenaPodatakaOGradu();
				break;
			case 10:
				GradUI.brisanjeGrada();
				break;
			default:
				System.out.println("Nepostojeća komanda");
				break;
			}
		}

		// zatvaranje konekcije, jednom na kraju aplikacije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Kulturne manifestacije:");
		System.out.println("\tOpcija broj 1 - Prikaz svih gradova");
		System.out.println("\tOpcija broj 2 - Prikaz svih kulturnih manifestacija");
		System.out.println("\tOpcija broj 3 - Pretraga kulturne manifestacije po identifikatoru");
		System.out.println("\tOpcija broj 4 - Izmena naziva manifestacije");
		System.out.println("\tOpcija broj 5 - Snimanje izvestaja o najposecenijoj manifestaciji");
		System.out.println("\tOpcija broj 6 - Citanje i ispis najposecenije manifestacije");
		System.out.println("\tOpcija broj 7 - Unos nove manifestacije");
		System.out.println("\tOpcija broj 8 - Brisanje manifestacije");
		System.out.println("\tOpcija broj 9 - Izmena podataka o gradu");
		System.out.println("\tOpcija broj 10 - Brisanje grada");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

	public static Connection getConn() {
		return conn;
	}
	
}
