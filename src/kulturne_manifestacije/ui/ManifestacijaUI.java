package kulturne_manifestacije.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import kulturne_manifestacije.dao.GradDAO;
import kulturne_manifestacije.dao.ManifestacijaDAO;
import kulturne_manifestacije.model.Grad;
import kulturne_manifestacije.model.Manifestacija;
import kulturne_manifestacije.utils.PomocnaKlasa;

public class ManifestacijaUI {
	
	public static void ispisSvihManifestacija() {
		List<Manifestacija> manifestacije = ManifestacijaDAO.getAll(ApplicationUI.getConn());
		
		System.out.println();
		System.out.println("Spisak manifestacija: ");
		System.out.println();
		System.out.printf("%5s %-30s %-15s %15s\n", "Id", "Naziv manifestacije", "Grad", "Broj posetilaca");
		System.out.println("===== ============================== =============== ===============");
		for (Manifestacija manifestacija : manifestacije) {
			System.out.printf("%5s %-30s %-15s %15s\n", manifestacija.getId(), manifestacija.getNaziv(),
					manifestacija.getGrad().getNaziv(), manifestacija.getBrojPosetilaca());
		}
		System.out.println("----- ------------------------------ --------------- ---------------");
	}
	
	public static void pretragaManifestacijePoId () {
		System.out.print("Unesite ID manifestacije:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		Manifestacija man = ManifestacijaDAO.getManifestacijaById(ApplicationUI.getConn(), id);
		if (man != null) {
			System.out.println();
			System.out.printf("%5s %-30s %-15s %15s\n", "Id", "Naziv manifestacije", "Grad", "Broj posetilaca");
			System.out.println("===== ============================== =============== ===============");
			System.out.printf("%5s %-30s %-15s %15s\n", man.getId(), man.getNaziv(), man.getGrad().getNaziv(),
								man.getBrojPosetilaca());
			System.out.println("----- ------------------------------ --------------- ---------------");
		} else 
			System.out.println("Ne postoji manifestacija sa tim ID-em.");
	}
	
	public static void izmenaNazivaManifestacije() {
		System.out.print("Unesite ID manifestacije:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		Manifestacija man = ManifestacijaDAO.getManifestacijaById(ApplicationUI.getConn(), id);
		if (man != null) {
			System.out.print("Unesite novi naziv manifestacije:");
			String naziv = PomocnaKlasa.ocitajTekst();
			man.setNaziv(naziv);
			if(ManifestacijaDAO.update(ApplicationUI.getConn(), man)) {
				System.out.println("Naziv manifestacije je promenjen.");
				return;
			}
			System.out.println("Doslo je do greske. Naziv manifestacije nije promenjen.");
			return;
		}
		System.out.println("Ne postoji manifestacija sa id-em: " + id);
	}
	
	public static void unosManifestacije() {
		System.out.print("Unesite naziv manifestacije:");
		String nazivManifestacije = PomocnaKlasa.ocitajTekst();
		System.out.print("Broj posetilaca: ");
		int brojPosetilaca = PomocnaKlasa.ocitajCeoBroj();
		
		Grad grad = null;
		while(grad == null) {
			System.out.println("Naziv grada:");
			String nazivGrada = PomocnaKlasa.ocitajTekst();
			grad = GradDAO.getGradByNaziv(ApplicationUI.getConn(), nazivGrada);
			if(grad == null)
				System.out.println("Grad ne postoji. Pokusajte ponovo.");
		}
		
		Manifestacija man = new Manifestacija(0, nazivManifestacije, brojPosetilaca, grad);
		if(ManifestacijaDAO.add(ApplicationUI.getConn(), man)) {
			System.out.println("Manifestacija je uneta u evidenciju.");
			return;
		}
		System.out.println("Doslo je do greske. Manifestacija nije uneta u bazu.");
	}
	
	public static void obrisiManifestaciju() {
		System.out.println("Unesi ID manifestacije:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		Manifestacija man = ManifestacijaDAO.getManifestacijaById(ApplicationUI.getConn(), id);
		if(man == null) {
			System.out.println("Manifestacija sa ID-em: " + id + " ne postoji u evidenciji.");
			return;
		}
		if(ManifestacijaDAO.delete(ApplicationUI.getConn(), id)) {
			System.out.println("Manifestacija je obrisana iz evidencije");
			return;
		}
		System.out.println("Doslo je do greske. Manifestacija nije obrisana.");
	}
	
	public static void ucitajNajposecenijuIzFajlaIIspisi(File dokument) throws IOException{
		if(dokument.exists()){

			BufferedReader in = new BufferedReader(new FileReader(dokument));
			in.mark(1); 
			if(in.read()!='\ufeff'){
				in.reset();
			}
			
			String s2;
			String [] tokeni = null; 
			while((s2 = in.readLine()) != null) {
				tokeni = s2.split(",");
			}
			in.close();
			
			System.out.println();
			System.out.println("Najposecanija manifestacija je: " + tokeni[0]);
			System.out.println("Broj posetilaca: " + tokeni[1]);
		} else {
			System.out.println("Ne postoji fajl!");
		}
	}
	
	public static void snimiNajposecenijuUFajl(File dokument) throws IOException{
		PrintWriter out2 = new PrintWriter(new FileWriter(dokument));
		Manifestacija man = ManifestacijaDAO.nadjiNajposecenijuManifestaciju(ApplicationUI.getConn());
		out2.println(man.getNaziv() + "," + man.getBrojPosetilaca());
		
		out2.flush();
		out2.close();
		
		System.out.println("Izvestaj je snimljen u fajl.");
	}
}
