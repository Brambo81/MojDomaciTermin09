package kulturne_manifestacije.model;

import java.util.ArrayList;
import java.util.List;

public class Grad {
	private String ptt;
	private String naziv;
	
	private List<Manifestacija> manifestacije = new ArrayList<>();

	public Grad(String ptt, String naziv) {
		super();
		this.ptt = ptt;
		this.naziv = naziv;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grad other = (Grad) obj;
		if (ptt == null) {
			if (other.ptt != null)
				return false;
		} else if (!ptt.equals(other.ptt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Grad [ptt=" + ptt + ", naziv=" + naziv + "]";
	}

	public String toStringAll() {
		return "Grad [ptt=" + ptt + ", naziv=" + naziv + ", manifestacije=" + manifestacije + "]";
	}

	public List<Manifestacija> getManifestacije() {
		return manifestacije;
	}

	public void setManifestacije(List<Manifestacija> manifestacije) {
		this.manifestacije = manifestacije;
	}

	public String getPtt() {
		return ptt;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	
}
