package kulturne_manifestacije.model;

public class Manifestacija {
	private int id;
	private String naziv;
	private int brojPosetilaca;
	private Grad grad;
	
	public Manifestacija(int id, String naziv, int brojPosetilaca, Grad grad) {
		this.id = id;
		this.naziv = naziv;
		this.brojPosetilaca = brojPosetilaca;
		this.grad = grad;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manifestacija other = (Manifestacija) obj;
		if (grad == null) {
			if (other.grad != null)
				return false;
		} else if (!grad.equals(other.grad))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Manifestacija [id=" + id + ", naziv=" + naziv + ", brojPosetilaca=" + brojPosetilaca + ", grad=" + grad
				+ "]";
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBrojPosetilaca() {
		return brojPosetilaca;
	}

	public void setBrojPosetilaca(int brojPosetilaca) {
		this.brojPosetilaca = brojPosetilaca;
	}

	public Grad getGrad() {
		return grad;
	}

	public void setGrad(Grad grad) {
		this.grad = grad;
	}

	public int getId() {
		return id;
	}
	
	
}
